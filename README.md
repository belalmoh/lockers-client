# Lockers client

## Steps for making things work:

```sh
$ git clone https://gitlab.com/belalmoh/lockers-client.git
$ cd lockers-client
$ yarn 
```

### After the above steps do their job, type: 
```sh
$ yarn start
```
### then navigate to http://localhost:3000 to see the client app.