import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { addLocker, getAllLockers } from '../HomePage/actions';
import { makeSelectLockers } from '../HomePage/selectors';
import LockersForm from './FeaturePage';

import reducer from '../HomePage/reducer';
import saga from '../HomePage/saga';

const mapDispatchToProps = (dispatch) => ({
  onSubmitForm: ({ userName, userLockerNumber }) => {
    dispatch(addLocker({ userName, userLockerNumber }));
  },
  onGetAllLockers: () => dispatch(getAllLockers())
});

const mapStateToProps = createStructuredSelector({
  lockers: makeSelectLockers()
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'home', reducer });
const withSaga = injectSaga({ key: 'home', saga });

export default compose(withReducer, withSaga, withConnect)(LockersForm);
export { mapDispatchToProps };
