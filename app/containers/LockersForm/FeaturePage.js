/*
 * FeaturePage
 *
 * List all the features
 */
import React from 'react';
import { Helmet } from 'react-helmet';
import { Col, Form, FormGroup, Label, Input, FormFeedback, Button, ButtonGroup } from 'reactstrap';
import './style.scss';

export default class FeaturePage extends React.Component {
  // eslint-disable-line react/prefer-stateless-function

  constructor(){
    super();
    this.state = {
      userLockerNumber: 0
    }
  }

  componentDidMount() {
    const { onGetAllLockers } = this.props;
    onGetAllLockers()
  }

  onChange = (userLockerNumber) => {
    const isInvalidLocker = this.props.lockers.indexOf(this.state.userLockerNumber);
    // console.log({isInvalidLocker})
    this.setState({userLockerNumber})
  }

  render() {
    const { history, lockers, onSubmitForm } = this.props;
    const isInvalidLocker = lockers.findIndex((locker) => locker.number === this.state.userLockerNumber) === -1 ? undefined : true;
    return (
      <div className="feature-page">
        <Helmet>
          <title>Add new locker</title>
        </Helmet>
        <h1>Add new locker</h1>
        <Form onSubmit={(e) => { 
          e.preventDefault();
          const userName = e.target['userName'].value;
          const userLockerNumber = e.target['userLockerNumber'].value;
          if(userName && userLockerNumber && !isInvalidLocker)
            onSubmitForm({userName, userLockerNumber})
        }}>
          <FormGroup row>
            <Label for="userName" sm={2} size="md">Reservee Name</Label>
            <Col sm={10}>
              <Input type="text" name="userName" id="userName" placeholder="Your name" bsSize="md" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="userLockerNumber" sm={2} size="md">Locker Number</Label>
            <Col sm={4}>
              <Input type="number" name="userLockerNumber" id="userLockerNumber" placeholder="Your locker number" bsSize="md" 
                onChange={(e) => { this.onChange(Number(e.target.value)); }} invalid={isInvalidLocker}/>
              <FormFeedback invalid={isInvalidLocker}>Uh oh! that locker is already reserved</FormFeedback>
            </Col>
          </FormGroup>
          <ButtonGroup>
            <Button outline color="danger" size="md" onClick={() => history.push('/')}>Cancel</Button>
            <Button outline color="success" size="md" >Submit</Button>
          </ButtonGroup>
        </Form>
      </div>
    );
  }
}
