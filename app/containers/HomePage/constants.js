/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const CHANGE_USERNAME = 'boilerplate/Home/CHANGE_USERNAME';

export const GET_ALL_LOCKERS = 'boilerplate/Home/GET_ALL_LOCKERS';
export const GET_ALL_LOCKERS_SUCCESS = 'boilerplate/Home/GET_ALL_LOCKERS_SUCCESS';
export const GET_ALL_LOCKERS_FAILED = 'boilerplate/Home/GET_ALL_LOCKERS_FAILED';

export const ADD_LOCKER = 'boilerplate/Home/ADD_LOCKER';
export const ADD_LOCKER_SUCCESS = 'boilerplate/Home/ADD_LOCKER_SUCCESS';
export const ADD_LOCKER_FAILED = 'boilerplate/Home/ADD_LOCKER_FAILED';
