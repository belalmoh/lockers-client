/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';

const selectHome = (state) => state.get('home');

const makeSelectUsername = () => createSelector(
  selectHome,
  (homeState) => homeState.get('username')
);


const makeSelectLockers = () => createSelector(
  selectHome,
  (lockersState) => lockersState.get('lockers')
);

export {
  selectHome,
  makeSelectUsername,

  selectLockers,
  makeSelectLockers
};
