/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import { Helmet } from 'react-helmet';
import { Button, Card, CardText, CardBody, CardTitle, CardSubtitle, Container } from 'reactstrap';
import { Column, Row } from 'simple-flexbox';
import './style.scss';

export default class HomePage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { onGetAllLockers } = this.props;
    onGetAllLockers()
  }
  
  render() {
    const {lockers, history} = this.props;
    return (
      <Container>
          <Helmet>
            <title>Home Page</title>
            <meta name="description" content="A React.js Boilerplate application homepage" />
          </Helmet>
          <div className="home-page">
            <Column flexGrow={1}>
              <Row horizontal='center'>
                <h1>Lockers Management System</h1>
              </Row>
              <Row horizontal='center'>
                <h5>Total reserved lockers: {lockers.length}</h5>
              </Row>
              <Row vertical='center'>
                {
                lockers.map((locker, index) => {
                  return (
                    <Column flexGrow={1} horizontal='center' key={`CARD-${index}`}>
                      <Card className="hover-button">
                        <CardBody>
                          <CardTitle>A reserved locker #{locker.number}</CardTitle>
                          <CardSubtitle className='hover-button--on'>By {locker.user_name}</CardSubtitle>
                          <CardSubtitle className='hover-button--off'></CardSubtitle>
                        </CardBody>
                      </Card>
                    </Column>
                  )
                })
              }
              </Row>
              <Row vertical='center'>
                <Button color="link" onClick={() => history.push('/lockers-form')}>Add new</Button>{' '}
              </Row>
              </Column>
          </div>
      </Container>
    );
  }
}
