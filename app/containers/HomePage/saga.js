/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest, all } from 'redux-saga/effects';
import axios from 'axios';
import { push } from 'react-router-redux'
import { LOAD_REPOS } from 'containers/App/constants';
import { 
  GET_ALL_LOCKERS,
  GET_ALL_LOCKERS_SUCCESS,
  GET_ALL_LOCKERS_FAILED,

  ADD_LOCKER,
  ADD_LOCKER_SUCCESS,
  ADD_LOCKER_FAILED
} from './constants';

import {
  getAllLockersSuccess,
  getAllLockersFailed,
  addLockerSuccess,
  addLockerFailed
} from './actions';

import { reposLoaded, repoLoadingError } from 'containers/App/actions';

import request from 'utils/request';
import { makeSelectUsername, makeSelectLockers } from 'containers/HomePage/selectors';

async function getLockers_Server() {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await axios.get('http://localhost:1337/api/locker/all', { crossdomain: true });
      return resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

async function addLocker_Server({userName, userLockerNumber}) {
  return new Promise(async (resolve, reject) => {
    try {
      const result = await axios.post('http://localhost:1337/api/locker/add', {
        userName, userLockerNumber
      }, { crossdomain: true });
      return resolve(result);
    } catch (err) {
      reject(err);
    }
  });
}

export function* getLockers() {
  try {
    const {data} = yield call(getLockers_Server);
    yield put(getAllLockersSuccess(data));
  } catch (err) {
    yield put(getAllLockersFailed('ERR'));
  }
}

export function* addLocker({data}) {
  const { userName, userLockerNumber } = data;
  try {
    const result = yield call(addLocker_Server, { userName, userLockerNumber });
    yield put(push('/'));
  } catch (err) {
    yield put(addLockerFailed("ERR"));
  }
}

/**
 * Github repos request/response handler
 */
export function* getRepos() {
  // Select username from store
  const username = yield select(makeSelectUsername());
  const requestURL = `https://api.github.com/users/${username}/repos?type=all&sort=updated`;

  try {
    // Call our request helper (see 'utils/request')
    const repos = yield call(request, requestURL);
    yield put(reposLoaded(repos, username));
  } catch (err) {
    yield put(repoLoadingError(err));
  }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* githubData() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  // yield all([
    yield takeLatest(GET_ALL_LOCKERS, getLockers);
    yield takeLatest(ADD_LOCKER, addLocker);
  // ]);
}
