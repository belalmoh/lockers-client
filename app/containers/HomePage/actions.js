/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import { 
  CHANGE_USERNAME, 
  ADD_LOCKER,
  ADD_LOCKER_SUCCESS,
  ADD_LOCKER_FAILED,
  GET_ALL_LOCKERS,
  GET_ALL_LOCKERS_SUCCESS,
  GET_ALL_LOCKERS_FAILED
} from './constants';

/**
 * Changes the input field of the form
 *
 * @param  {name} name The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_USERNAME
 */
export function changeUsername(name) {
  return {
    type: CHANGE_USERNAME,
    name
  };
}

export function addLocker(locker) {
  return {
    type: ADD_LOCKER,
    data: locker
  };
}

export function addLockerSuccess() {
  return {
    type: ADD_LOCKER_SUCCESS
  };
}

export function addLockerFailed(error) {
  return {
    type: ADD_LOCKER_FAILED,
    error
  };
}

export function getAllLockers() {
  return {
    type: GET_ALL_LOCKERS
  };
}

export function getAllLockersSuccess(allLockers) {
  return {
    type: GET_ALL_LOCKERS_SUCCESS,
    data: allLockers
  };
}

export function getAllLockersFailed(error) {
  return {
    type: GET_ALL_LOCKERS_FAILED,
    error
  };
}
